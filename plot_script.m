%%
clear all; close all;

%fname = 'piston';
fname = 'otlcircuit';

load(sprintf('%s_sensitivity_error_study.mat',fname));

if strcmp(fname, 'piston')
    labels = {'$M$', '$S$', '$V_0$', '$k$', '$P_0$', '$T_a$', '$T_0$'};
    err_ymin = 1e-8; err_ymax = 1e0;
    stderr_ymin = 1e-8; stderr_ymax = 1e0;
else
    labels = {'$R_{b1}$', '$R_{b2}$', '$R_f$', '$R_{c1}$', '$R_{c2}$', '$\beta$'};
    err_ymin = 1e-9; err_ymax = 1e0;
    stderr_ymin = 1e-9; stderr_ymax = 1e0;
end

pp3 = 3; pp4 = 3;

%%
% tau plots
f1 = figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

ll = loglog(M, tau_err', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Relative error');
xlim([min(M) max(M)]); 
ylim([err_ymin err_ymax]);
print(sprintf('figs/%s/tau_err', fname), '-depsc2');

f2 = figure;
f2.PaperPositionMode = 'auto';
legendflex(ll, labels, 'ref', f2, 'interpreter', 'latex', ...
    'anchor', {'ne','ne'}, 'padding', [8 8 8]);
print(sprintf('figs/%s/legend', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, tau_stderr', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Bootstrap standard error');
xlim([min(M) max(M)]); 
ylim([stderr_ymin stderr_ymax]);
print(sprintf('figs/%s/tau_stderr', fname), '-depsc2');

%%
% dgsm plots
figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, nu_err', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Relative error');
xlim([min(M) max(M)]); 
ylim([err_ymin err_ymax]);
print(sprintf('figs/%s/dgsm_err', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, nu_stderr', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Standard error');
xlim([min(M) max(M)]); 
ylim([stderr_ymin stderr_ymax]);
print(sprintf('figs/%s/dgsm_stderr', fname), '-depsc2');

%%
% beta plots
figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, beta_err', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Relative error');
xlim([min(M) max(M)]); 
ylim([err_ymin err_ymax]);
print(sprintf('figs/%s/beta_err', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, beta_stderr', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Bootstrap standard error');
xlim([min(M) max(M)]); 
ylim([stderr_ymin stderr_ymax]);
print(sprintf('figs/%s/beta_stderr', fname), '-depsc2');

%%
% w1 plots
figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, w1_err', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Relative error');
xlim([min(M) max(M)]); 
ylim([err_ymin err_ymax]);
print(sprintf('figs/%s/w1_err', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, w1_stderr', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Bootstrap standard error');
xlim([min(M) max(M)]); 
ylim([stderr_ymin stderr_ymax]);
print(sprintf('figs/%s/w1_stderr', fname), '-depsc2');

%%
% alpha plots
figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, alpha_err', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Relative error');
xlim([min(M) max(M)]); 
ylim([err_ymin err_ymax]);

%le = legend(labels,'interpreter','latex','Location','bestoutside');
%le.FontSize = 14;

print(sprintf('figs/%s/alpha_err', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = pp3; pp(4) = pp4;
set(gcf, 'PaperPosition', pp);

loglog(M, alpha_stderr', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Number of MC samples'); ylabel('Bootstrap standard error');
xlim([min(M) max(M)]); 
ylim([stderr_ymin stderr_ymax]);

%le = legend(labels,'interpreter','latex','Location','bestoutside');
%le.FontSize = 14;

print(sprintf('figs/%s/alpha_stderr', fname), '-depsc2');
