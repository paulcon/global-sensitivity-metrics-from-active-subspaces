function alpha = act_scores_quad(dfun, m, n, dim)
% dfun is anonymous function that returns a gradient
% m is number of input parameters
% n number of points per dimension
% dim is dimension of active subspace
% alpha is the activitiy score 

s = [];
for i=1:m, s = [s; parameter()]; end

order = n*ones(1,m);
[p,w] = gaussian_quadrature(s, order);
DF = dfun(p);
[~,Sig,W] = svd(bsxfun(@times, DF, sqrt(w)), 'econ');
lambda = diag(Sig).^2;
W = bsxfun(@times, W, sign(W(1,:)));

% compute activity scores
if length(dim) > 1
    alpha = zeros(m,length(dim));
    for i=1:length(dim)
        alpha(:,i) = W(:,1:dim(i)).^2*lambda(1:dim(i));
    end
else
    alpha = W(:,1:dim).^2*lambda(1:dim);
end
