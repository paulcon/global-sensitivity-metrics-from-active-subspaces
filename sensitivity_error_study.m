function [err, stderr] = sensitivity_error_study(fun, m, M, quad_method, mc_method)
% Sobol index error study

% truth values
n = 7;
est_q = quad_method(fun, m, n);

err = zeros(m, length(M));
stderr = zeros(m, length(M), 1);
ntrials = 50;

for i=1:length(M)
   
    tic;
    
    % average error over several trials
    et = zeros(m, ntrials);
    st = zeros(m, ntrials);
    for j=1:ntrials
        [est_mc, se_est] = mc_method(fun, m, M(i));
        st(:,j) = se_est ./ max(abs(est_mc));
        et(:,j) = abs(est_mc - est_q) ./ max(abs(est_q));
    end
    err(:,i) = mean(et,2);
    stderr(:,i) = mean(st,2);
    
    fprintf('%d of %d, time %4.2f sec\n',i,length(M),toc);
    
end