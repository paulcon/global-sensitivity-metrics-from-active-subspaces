function dCC = d_piston(X)

xl = [30 0.005 0.002 1000 90000 290 340];
xu = [60 0.020 0.010 5000 110000 296 360];

% shift and scale
XX = bsxfun(@plus, bsxfun(@times, 0.5*(X + 1), xu - xl), xl);

M = XX(:,1); S = XX(:,2); V0 = XX(:,3); k = XX(:,4);
P0 = XX(:,5); Ta = XX(:,6); T0 = XX(:,7);

dC = zeros(size(X));

dC(:,1) = (k * pi .* (1 + (4 * k .* P0 .* Ta .* V0) ./ (T0 .* (-19.62 * M - ...
    P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2) - (156.96 .* k .* ...
    M .* P0 .* S.^2 .* Ta .* V0) ./ (T0 .* sqrt((4 * k .* P0 .* Ta .* V0) ./ ...
    T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2) .* (19.62 * M .* S + ...
    P0 .* S.^2 - k .* V0 - S .* sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2))) ./ (sqrt( M ./ (k + ...
    (4 * k.^2 .* P0 .* Ta .* V0) ./ (T0 .* (-19.62 * M - P0 .* S + ...
    (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + ...
    P0 .* S - (k .* V0) ./ S).^2)).^2))) .* (k + (4 * k.^2 .* P0 .* Ta .* V0) ./ ...
    (T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* ...
    Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2)).^2);

dC(:,2) = (8 * k .* P0 * pi .* Ta .* V0 .* (-P0 - (k .* V0) ./ S.^2 + ...
    ((P0 + (k .* V0) ./ S.^2) .* (19.62 * M + P0 .* S - (k .* V0) ./ S)) ./ ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)) .* sqrt( M ./ (k + (4 * k.^2 .* P0 .* Ta .* V0) ./ ...
    (T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* ...
    Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2)))) ./ ...
    ((-19.62 * M - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* ...
    V0) ./ T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^3 .* (T0 + ...
    (4 * k .* P0 .* Ta .* V0) ./ (-19.62 * M - P0 .* S + (k .* V0) ./ ...
    S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^2));

dC(:,3) = -((4 * k .* P0 .* pi .* Ta .* (-19.62 * M - P0 .* S + ...
    (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2) - (2 * k .* V0 .* ...
    (S + (-19.62 * M .* S .* T0 + P0 .* S.^2 .* (-T0 + 2 * Ta) + k .* ...
    T0 .* V0) ./ (T0 .* sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)))) ./ S.^2) .* ...
    sqrt( M./ (k + (4 * k.^2 .* P0 .* Ta .* V0) ./ (T0 .* (-19.62 * M ...
    - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ ...
    T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2)))) ./ ...
    ((-19.62 * M - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* ...
    V0) ./ T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^3 .* ...
    (T0 + (4 * k .* P0 .* Ta .* V0) ./ (-19.62 * M - P0 .* S + ...
    (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2)));

dC(:,4) = -(pi ./ M) .* (1 - (8 * k.^2 .* P0 .* Ta .* V0.^2 .* (S + ...
    (-19.62 * M .* S .* T0 + P0 .* S.^2 .* (-T0 + 2 * Ta) + k .* T0 .* ...
    V0) ./ (T0 .* sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + ...
    P0 .* S - (k .* V0) ./ S).^2)))) ./ (S.^2 .* T0 .* (-19.62 * M - ...
    P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^3) + (8 * k .* P0 .* ...
    Ta .* V0) ./ (T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + ... 
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^2)) .* (M ./ (k + (4 * k.^2 .* P0 .* Ta .* ...
    V0) ./ (T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^2))).^(1.5);

dC(:,5) = -((4 * k .* pi .* Ta .* V0 .* (-19.62 * M - P0 .* S + ...
    (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + ...
    P0 .* S - (k .* V0) ./ S).^2) - 2 * P0 .* (-S + (19.62 * M .* S .* ...
    T0 + P0 .* S.^2 .* T0 + k .* (-T0 + 2 * Ta) .* V0) ./ (T0 .* ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)))) .* sqrt( M ./ (k + (4 * k.^2 .* P0 .* ...
    Ta .* V0) ./ (T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^2)))) ./ ((-19.62 * M - P0 .* S + (k .* V0) ./ ...
    S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^3 .* (T0 + (4 * k .* P0 .* Ta .* V0) ./ ...
    (-19.62 * M - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* ...
    V0) ./ T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2)));

dC(:,6) = -((4 * M .* P0 .* pi .* V0 .* (-((4 * k .* P0 .* Ta .* V0) ./ ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - (k .* ...
    V0) ./ S).^2)) + T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)))) ./ ((-19.62 * M - P0 .* S + (k .* V0) ./ S + ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^3 .* (T0 + (4 * k .* P0 .* Ta .* V0) ./ ...
    (-19.62 * M - P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* ...
    V0) ./ T0 + (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2).^2 .* ...
    sqrt( M ./ (k + (4 * k.^2 .* P0 .* Ta .* V0) ./ (T0 .* (-19.62 * M - ...
    P0 .* S + (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^2)))));

dC(:,7) = -((4 * k.^2 .* P0 .* pi .* Ta .* V0 .* (M ./ (k + (4 * k.^2 .* ...
    P0 .* Ta .* V0) ./ (T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)).^2))).^(1.5) .* ((4 * k .* P0 .* Ta .* V0) ./ ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2) - T0 .* (-19.62 * M - P0 .* S + (k .* V0) ./ S + ...
    sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + (19.62 * M + P0 .* S - ...
    (k .* V0) ./ S).^2)))) ./ (M .* T0.^3 .* (-19.62 * M - P0 .* S + ...
    (k .* V0) ./ S + sqrt((4 * k .* P0 .* Ta .* V0) ./ T0 + ...
    (19.62 * M + P0 .* S - (k .* V0) ./ S).^2)).^3));

dCC = bsxfun(@times, dC, 0.5*(xu - xl) );