%%
% Test script
close all; clear all;

%%
fun = @piston
tau_q = sobol_tsi_quad(fun, 7, 3);
[tau_mc, se_tau] = sobol_tsi_mc(fun, 7, 10000);

%%
beta_q = linear_coeffs_quad(fun, 7, 3);
[beta_mc, se_beta] = linear_coeffs_mc(fun, 7, 10000);

%%
dfun = @d_piston
nu_q = dgsm_quad(dfun, 7, 3);
[nu_mc, se_nu] = dgsm_mc(dfun, 7, 10000);

%%
dfun = @d_piston
[alpha_q, w_q] = act_scores_quad(dfun, 7, 7, 1);
[alpha_mc, w_mc, se_alpha, se_w] = act_scores_mc(dfun, 7, 10000, 1);