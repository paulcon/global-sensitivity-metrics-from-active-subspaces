function w = first_evec_quad(dfun, m, n)
% dfun is anonymous function that returns a gradient
% m is number of input parameters
% n number of points per dimension
% w is the first eigenvector

s = [];
for i=1:m, s = [s; parameter()]; end

order = n*ones(1,m);
[p,w] = gaussian_quadrature(s, order);
DF = dfun(p);
[~,~,W] = svd(bsxfun(@times, DF, sqrt(w)), 'econ');
W = bsxfun(@times, W, sign(W(1,:)));

w = W(:,1);
