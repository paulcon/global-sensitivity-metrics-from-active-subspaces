function nu = dgsm_quad(dfun, m, n)
% dfun is anonymous function that returns a gradient
% m is number of input parameters
% n number of points per dimension
% nu is the mean squared partial derivative 

s = [];
for i=1:m, s = [s; parameter()]; end

order = n*ones(1,m);
[p,w] = gaussian_quadrature(s, order);
DF2 = dfun(p).^2;
nu = (DF2)'*w;

