function [beta_hat, se] = linear_coeffs_mc(fun, m, M)
    
X = 2*rand(M,m) - 1;
f = fun(X);

%compute regression coefficients
A = [ones(M,1) X];
b = A\f; 
beta_hat = b(2:m+1)/(sqrt(3)*std(f));

% bootstrap
Nboot = 100;
bstarz = zeros(m,Nboot);
for i=1:Nboot    
    indz = randi(M,1,M);
    Astar = A(indz,:); fstar = f(indz); 
    if rank(Astar) == m+1
        b = Astar\fstar;
        bstarz(:,i) = b(2:m+1)/(sqrt(3)*std(f));
    end
end

ind = sum(abs(bstarz),1)==0;
bstarz(:,ind) = [];
se = std(bstarz,0,2)/sqrt(size(bstarz,2));
    
end 