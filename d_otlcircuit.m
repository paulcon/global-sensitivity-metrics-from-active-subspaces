function dVV = d_otlcircuit(X)

xl = [50 25 0.5 1.2 0.25 50];
xu = [150 70 3 2.5 1.2 300];

% shift and scale
XX = bsxfun(@plus, bsxfun(@times, 0.5*(X + 1), xu - xl), xl);

Rb1 = XX(:,1); Rb2 = XX(:,2); Rf = XX(:,3); 
Rc1 = XX(:,4); Rc2 = XX(:,5); beta = XX(:,6);

dV = zeros(size(X));

dV(:,1) = ( -12 * Rb2 .* beta .* (Rc2 + 9) ) ./ ...
    ( (beta.*(Rc2 + 9) + Rf ) .* (Rb1 + Rb2).^2 );

dV(:,2) = ( 12 * Rb1 .* beta .* (Rc2 + 9) ) ./ ...
    ( (beta.*(Rc2 + 9) + Rf ) .* (Rb1 + Rb2).^2 );

dV(:,3) = (beta .* (beta .* (Rb1 + Rb2) .* (59.94 + 13.32 * Rc2 + 0.74 * Rc2.^2) + ...
    Rc1 .* (Rb2 .* (-12.51 - 1.39 * Rc2) + Rb1 .* (95.49 + 10.61 * Rc2)))) ./ ...
    ((Rb1 + Rb2) .* Rc1 .* (beta .* (9 + Rc2) + Rf).^2);
 
dV(:,4) = -( 0.74 * beta .* (Rc2 + 9) .* Rf ) ./ ...
     ( Rc1.^2 .* (beta .* (Rc2 + 9) + Rf ) );
 
dV(:,5) = (beta .* Rf .* (-10.61 * Rb1 .* Rc1 + 1.39 * Rb2 .* Rc1 + ...
     0.74 * Rb1 .* Rf + 0.74 * Rb2 .* Rf)) ./ ((Rb1 + Rb2) .* Rc1 .* ...
     (beta .* (9 + Rc2) + Rf).^2);
 
dV(:,6) = (Rf .* (Rb1 .* (-95.49 * Rc1 - 10.61 * Rc1 .* Rc2 + 6.66 * Rf + ...
    0.74 * Rc2 .* Rf) + Rb2 .* (12.51 * Rc1 + 1.39 * Rc1 .* Rc2 + ...
    6.66 * Rf + 0.74 * Rc2 .* Rf))) ./ ((Rb1 + Rb2) .* Rc1 .* (beta .* ...
    (9 + Rc2) + Rf).^2);
 
dVV = bsxfun(@times, dV, 0.5*(xu - xl) );
 
 