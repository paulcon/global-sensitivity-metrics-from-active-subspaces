function [tau, se] = sobol_tsi_mc(fun, m, M)
% fun is anonymous function
% m is number of input parameters
% M number of monte carlo points
% tau is the total sensitivity index
% se is the standard deviation error 

N = floor( M / (1+m) );
X = 2*rand(2*N, m) - 1;
tau = tsi_mc(X, fun);

% bootstrap
Nboot = 100;
tau_starz = zeros(m,Nboot);
for i=1:Nboot    
    indz = randi(2*N,1,2*N);
    Xstar = X(indz,:);
    tau_starz(:,i) = tsi_mc(Xstar, fun);
end
se = std(tau_starz,0,2)/sqrt(Nboot);

end

function tau = tsi_mc(X, fun)

N = size(X,1)/2;
m = size(X,2);

% A samples
A = X(1:N,:);
fA = fun(A);
V = var(fA);

% B samples
B = X(N+1:2*N,:);

tau = zeros(m,1);
for i=1:m

    ABi = A; ABi(:,i) = B(:,i);
    g = fA - fun(ABi);
    tau(i) = (g'*g)/(2*N*V);
    
end


end

