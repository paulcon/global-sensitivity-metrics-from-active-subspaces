function [w, se_w] = first_evec_mc(dfun, m, M)
% dfun is anonymous function that returns a gradient
% m is number of input parameters
% n number of points per dimension
% w is the first eigenvector

X = 2*rand(M, m) - 1;
DF = dfun(X);
w = wscores(DF);

% bootstrap
Nboot = 100;
wstarz = zeros(m,Nboot);
for i=1:Nboot    
    indz = randi(M,1,M);
    DFstar = DF(indz,:);
    wstarz(:,i) = wscores(DFstar);
end
se_w = std(wstarz,0,2)/sqrt(Nboot);

end

function w = wscores(DF)

% eigenvalues and eigenvectors
[~, ~, W] = svd(DF, 'econ');
w = W(:,1)*sign(W(1,1));

end