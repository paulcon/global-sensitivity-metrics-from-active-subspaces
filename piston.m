function C = piston(X)

xl = [30 0.005 0.002 1000 90000 290 340];
xu = [60 0.020 0.010 5000 110000 296 360];

% shift and scale
XX = bsxfun(@plus, bsxfun(@times, 0.5*(X + 1), xu - xl), xl);

M = XX(:,1); S = XX(:,2); V0 = XX(:,3); k = XX(:,4);
P0 = XX(:,5); Ta = XX(:,6); T0 = XX(:,7);

A = P0.*S + 19.62*M - (k.*V0 ./ S);
V = (S ./ (2*k) ) .* (sqrt(A.^2 + ((4*k.*Ta.*P0.*V0)./(T0))) - A);
C = 2*pi*sqrt( M ./ (k + (S.*S.*P0.*V0.*Ta) ./ (T0.*V.*V) ));