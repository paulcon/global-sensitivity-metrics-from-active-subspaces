%%
clear all; close all;

%%
% set up function

fname = 'piston';
%fname = 'otlcircuit';

if strcmp(fname, 'piston')
    labels = {'$M$', '$S$', '$V_0$', '$k$', '$P_0$', '$T_a$', '$T_0$'};
    fun = @piston; dfun = @d_piston; m = 7; 
else
    labels = {'$R_{b1}$', '$R_{b2}$', '$R_f$', '$R_{c1}$', '$R_{c2}$', '$\beta$'};
    fun = @otlcircuit; dfun = @d_otlcircuit; m = 6; 
end

mlabels = {'$\tau$', '$\nu$', '$\beta$', '$\mathbf{w}_1$', '$\alpha$'};
n = 7;

r = zeros(m,5);

tau = sobol_tsi_quad(fun, m, n);
r(:,1) = abs(tau) / norm(tau);

nu = dgsm_quad(dfun, m, n);
r(:,2) = abs(nu) / norm(nu);

beta = linear_coeffs_quad(fun, m, n);
r(:,3) = abs(beta) / norm(beta);

w = first_evec_quad(dfun, m, n);
r(:,4) = abs(w) / norm(w);

alpha = act_scores_quad(dfun, m, n, 1);
r(:,5) = abs(alpha) / norm(alpha);

%%
figure(1);
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);

plot(1:m, r, 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Parameter');
ax = gca;
ax.XTick = 1:m;
ax.XTickLabel = labels;
ax.XTickLabelRotation = 0;
ax.TickLabelInterpreter = 'latex';
ylabel('Sensitivity ranking');
legend(mlabels,'interpreter','latex','location','northeast');
xlim([1 m]); ylim([0 1]);
print(sprintf('figs/%s/rankings', fname), '-depsc2');

