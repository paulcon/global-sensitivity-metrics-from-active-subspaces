function beta = linear_coeffs_quad(fun, m, N)

s = [];
for i=1:m, s=[s; parameter()]; end
order = N*ones(1,m);
P = pseudospectral(fun,s,order);
w = fliplr(P.coefficients(2:m+1))';
sigma = norm(P.coefficients(2:end));
beta = w/sigma;

