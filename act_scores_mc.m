function [alpha, se_alpha] = act_scores_mc(dfun, m, M, dim)
% dfun is anonymous function that returns a gradient
% m is number of input parameters
% n number of points per dimension
% dim is dimension of active subspace
% alpha is the activitiy score 

X = 2*rand(M, m) - 1;
DF = dfun(X);
alpha = ascores(DF, dim);

% bootstrap
Nboot = 100;
astarz = zeros(m,Nboot);
for i=1:Nboot    
    indz = randi(M,1,M);
    DFstar = DF(indz,:);
    astarz(:,i) = ascores(DFstar, dim);
end
se_alpha = std(astarz,0,2)/sqrt(Nboot);

end

function a = ascores(DF, dim)
    
M = size(DF,1);

% eigenvalues and eigenvectors
[~, Sigma, W] = svd(DF, 'econ');
lambda = diag(Sigma).^2/M;
W = bsxfun(@times, W, sign(W(1,:)));

% compute activity scores
a = W(:,1:dim).^2*lambda(1:dim);

end