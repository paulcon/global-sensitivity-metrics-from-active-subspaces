%%
close all; clear all;

%%

% set up function
%fun = @piston; dfun = @d_piston; m = 7; fname = 'piston';
fun = @otlcircuit; dfun = @d_otlcircuit; m = 6; fname = 'otlcircuit';

% points to compute errors
M = [50 100 500 1000 5000 10000 50000];

%%
% total sensitivity indices
[tau_err, tau_stderr] = sensitivity_error_study(fun, m, M, ...
    @sobol_tsi_quad, @sobol_tsi_mc);

% dgsm
[nu_err, nu_stderr] = sensitivity_error_study(dfun, m, M, ...
    @dgsm_quad, @dgsm_mc);

% linear coefficients
[beta_err, beta_stderr] = sensitivity_error_study(fun, m, M, ...
    @linear_coeffs_quad, @linear_coeffs_mc);

% first eigenvector
[w1_err, w1_stderr] = sensitivity_error_study(dfun, m, M, ...
    @first_evec_quad, @first_evec_mc);

% activity scores
dim = 1;
quad_method = @(fun,m,n) act_scores_quad(dfun,m,n,dim);
mc_method = @(fun,m,M) act_scores_mc(dfun,m,M,dim);
[alpha_err, alpha_stderr] = sensitivity_error_study(fun, m, M, ...
    quad_method, mc_method);

save(sprintf('%s_sensitivity_error_study.mat',fname), 'M', ...
    'tau_err', 'tau_stderr', 'nu_err', 'nu_stderr', ...
    'beta_err', 'beta_stderr', 'w1_err', 'w1_stderr', ...
    'alpha_err', 'alpha_stderr');

