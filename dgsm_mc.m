function [nu, se] = dgsm_mc(dfun, m, M)
% dfun is anonymous function that returns a gradient
% m is number of input parameters
% n number of points per dimension
% nu is the mean squared partial derivative 
% se is the standard deviation error 

X = 2*rand(M,m) - 1;
DF2 = dfun(X).^2;
nu = sum(DF2,1)'/M;
se = std(DF2,0,1)'/sqrt(M);


