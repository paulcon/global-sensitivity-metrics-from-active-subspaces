%%
% active subspace and activity scores
close all; clear all;

%%
fname = 'piston';
%fname = 'otlcircuit';

if strcmp(fname, 'piston')
    labels = {'$M$', '$S$', '$V_0$', '$k$', '$P_0$', '$T_a$', '$T_0$'};
    fun = @piston; dfun = @d_piston; m = 7; 
    outlabel = 'Piston cycle time [s]';
else
    labels = {'$R_{b1}$', '$R_{b2}$', '$R_f$', '$R_{c1}$', '$R_{c2}$', '$\beta$'};
    fun = @otlcircuit; dfun = @d_otlcircuit; m = 6;
    outlabel = 'Circuit voltage [V]';
end

%%
s = [];
for i=1:m, s = [s; parameter()]; end

n = 7;
order = n*ones(1,m);
[p,w] = gaussian_quadrature(s, order);
DF = dfun(p);
[~,Sig,W] = svd(bsxfun(@times, DF, sqrt(w)), 'econ');
lambda = diag(Sig).^2;
W = bsxfun(@times, W, sign(W(1,:)));

%%
figure;
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);

semilogy(1:m, lambda, 'bo-', ...
    'MarkerSize', 10, 'LineWidth', 2);
axis square; grid on;
xlabel('Index'); ylabel('Eigenvalue');
xlim([1 m]); 
%ylim([err_ymin err_ymax]);
print(sprintf('figs/%s/as_evals', fname), '-depsc2');

M = 500;
X = 2*rand(M,m) - 1;
F = fun(X);

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);

plot(X*W(:,1), F, 'ko', 'LineWidth', 2, 'MarkerFaceColor', 'r', 'MarkerSize', 10);
axis square; grid on;
xlabel('$\mathbf{w}_1^T\,\mathbf{x}$','interpreter','latex'); 
ylabel(outlabel); 
xlim([-2 2]); 
%ylim([err_ymin err_ymax]);
print(sprintf('figs/%s/as_ssp1', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);

scatter(X*W(:,1), X*W(:,2), 80, F, 'filled');
view(2);
axis square; grid on;
xlabel('$\mathbf{w}_1^T\,\mathbf{x}$','interpreter','latex'); 
ylabel('$\mathbf{w}_2^T\,\mathbf{x}$','interpreter','latex'); 
xlim([-2 2]); ylim([-2 2]);
colorbar;
title(outlabel);
print(sprintf('figs/%s/as_ssp2', fname), '-depsc2');

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);

plot(1:m, W(:,1), 'ko-', 1:m, W(:,2), 'kx-', ...
    'MarkerSize', 10, 'LineWidth', 2);
axis square; grid on;
xlabel('Parameter'); 
ax = gca;
ax.XTick = 1:m;
ax.XTickLabel = labels;
ax.XTickLabelRotation = 0;
ax.TickLabelInterpreter = 'latex';
ylabel('Eigenvector component'); 
legend({'$\mathbf{w}_1$', '$\mathbf{w}_2$'}, ...
    'Location','NorthEast','interpreter', 'latex');
xlim([1 m]); ylim([-1 1]);
print(sprintf('figs/%s/as_evecs', fname), '-depsc2');

%%
% activity scores

alpha = act_scores_quad(dfun, m, n, 1:m);

figure;
pp = get(gcf, 'PaperPosition');
pp(3) = 4; pp(4) = 4;
set(gcf, 'PaperPosition', pp);

plot(1:m, alpha', 'o-', 'LineWidth', 2);
axis square; grid on;
xlabel('Active subspace dimension'); ylabel('Activity score');
xlim([1 m]); 
ylim([0 1.1*max(max(alpha))]);
legend(labels, 'interpreter', 'latex', 'Location', 'Bestoutside');
print(sprintf('figs/%s/alphan', fname), '-depsc2');



