%%
close all; clear all;

%% 
% Estimate finite differences

m = 7; N = 100;
X = 2*rand(N, m) + 1;

% derivative
dV = d_piston(X);

% finite diff
hz = 2.^(-(4:20));
errz = zeros(size(hz));

for i=1:length(hz)
    h = hz(i);
    dV_fd = zeros(size(dV));

    for j=1:m
        Xp = X; Xp(:,j) = Xp(:,j) + h;
        dV_fd(:,j) = (piston(Xp) - piston(X)) / h;
    end

    % difference
    errz(i) = norm(dV - dV_fd);

end

%%
figure(1);
loglog(hz, errz, 'bo-');
axis square; grid on;
xlabel('h'); ylabel('error');
title('Piston');

%% 
% Estimate finite differences

m = 6; N = 100;
X = 2*rand(N, m) + 1;

% derivative
dV = d_otlcircuit(X);

% finite diff
hz = 2.^(-(4:20));
errz = zeros(size(hz));

for i=1:length(hz)
    h = hz(i);
    dV_fd = zeros(size(dV));

    for j=1:m
        Xp = X; Xp(:,j) = Xp(:,j) + h;
        dV_fd(:,j) = (otlcircuit(Xp) - otlcircuit(X)) / h;
    end

    % difference
    errz(i) = norm(dV - dV_fd);

end

%%
figure(2);
loglog(hz, errz, 'bo-');
axis square; grid on;
xlabel('h'); ylabel('error');
title('OTL Circuit');